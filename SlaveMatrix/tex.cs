﻿using System;
using System.Linq;
using _2DGAMELIB;

namespace SlaveMatrix
{
	public static class tex
	{
		public static string アフ\u30FCル
		{
			get
			{
				return tex.Race[0];
			}
		}

		public static string アラクネ
		{
			get
			{
				return tex.Race[1];
			}
		}

		public static string アリコ\u30FCン
		{
			get
			{
				return tex.Race[2];
			}
		}

		public static string アルラウネ
		{
			get
			{
				return tex.Race[3];
			}
		}

		public static string イクテュオケンタウレ
		{
			get
			{
				return tex.Race[4];
			}
		}

		public static string ヴィオランテ
		{
			get
			{
				return tex.Race[5];
			}
		}

		public static string ウェアウルフ
		{
			get
			{
				return tex.Race[6];
			}
		}

		public static string ウェアキャット
		{
			get
			{
				return tex.Race[7];
			}
		}

		public static string ウェアスタッグビ\u30FCトル
		{
			get
			{
				return tex.Race[8];
			}
		}

		public static string ウェアドラゴンフライ
		{
			get
			{
				return tex.Race[9];
			}
		}

		public static string ウェアビ\u30FCトル
		{
			get
			{
				return tex.Race[10];
			}
		}

		public static string ウェアフォックス
		{
			get
			{
				return tex.Race[11];
			}
		}

		public static string ウェアマンティス
		{
			get
			{
				return tex.Race[12];
			}
		}

		public static string ウロボロス
		{
			get
			{
				return tex.Race[13];
			}
		}

		public static string エイリアン
		{
			get
			{
				return tex.Race[14];
			}
		}

		public static string エキドナ
		{
			get
			{
				return tex.Race[15];
			}
		}

		public static string エルフ
		{
			get
			{
				return tex.Race[16];
			}
		}

		public static string エンジェル
		{
			get
			{
				return tex.Race[17];
			}
		}

		public static string オ\u30FCグリス
		{
			get
			{
				return tex.Race[18];
			}
		}

		public static string オ\u30FCルドスキュラ
		{
			get
			{
				return tex.Race[19];
			}
		}

		public static string オ\u30FCルドマ\u30FCメイド
		{
			get
			{
				return tex.Race[20];
			}
		}

		public static string オノケンタウレ
		{
			get
			{
				return tex.Race[21];
			}
		}

		public static string カ\u30FCバンクル
		{
			get
			{
				return tex.Race[22];
			}
		}

		public static string カッパ
		{
			get
			{
				return tex.Race[23];
			}
		}

		public static string カトブレパス
		{
			get
			{
				return tex.Race[24];
			}
		}

		public static string カプラケンタウレ
		{
			get
			{
				return tex.Race[25];
			}
		}

		public static string カリュブディス
		{
			get
			{
				return tex.Race[26];
			}
		}

		public static string キマイラ
		{
			get
			{
				return tex.Race[27];
			}
		}

		public static string ギルタブリル
		{
			get
			{
				return tex.Race[28];
			}
		}

		public static string ギルタブルル
		{
			get
			{
				return tex.Race[29];
			}
		}

		public static string クラ\u30FCケン
		{
			get
			{
				return tex.Race[30];
			}
		}

		public static string グリフォン
		{
			get
			{
				return tex.Race[31];
			}
		}

		public static string コカトリス
		{
			get
			{
				return tex.Race[32];
			}
		}

		public static string ゴルゴン
		{
			get
			{
				return tex.Race[33];
			}
		}

		public static string サイクロプス
		{
			get
			{
				return tex.Race[34];
			}
		}

		public static string サキュバス
		{
			get
			{
				return tex.Race[35];
			}
		}

		public static string サンドワ\u30FCム
		{
			get
			{
				return tex.Race[36];
			}
		}

		public static string シ\u30FCラミア
		{
			get
			{
				return tex.Race[37];
			}
		}

		public static string スキュラ
		{
			get
			{
				return tex.Race[38];
			}
		}

		public static string スフィンクス
		{
			get
			{
				return tex.Race[39];
			}
		}

		public static string スライム
		{
			get
			{
				return tex.Race[40];
			}
		}

		public static string セイレ\u30FCン
		{
			get
			{
				return tex.Race[41];
			}
		}

		public static string チ\u30FCタケンタウレ
		{
			get
			{
				return tex.Race[42];
			}
		}

		public static string ティグリスケンタウレ
		{
			get
			{
				return tex.Race[43];
			}
		}

		public static string デビル
		{
			get
			{
				return tex.Race[44];
			}
		}

		public static string デルピヌスケンタウレ
		{
			get
			{
				return tex.Race[45];
			}
		}

		public static string ドラコケンタウレ
		{
			get
			{
				return tex.Race[46];
			}
		}

		public static string ドラゴニュ\u30FCト
		{
			get
			{
				return tex.Race[47];
			}
		}

		public static string ドラゴン
		{
			get
			{
				return tex.Race[48];
			}
		}

		public static string ドルフィンマ\u30FCメイド
		{
			get
			{
				return tex.Race[49];
			}
		}

		public static string ドワ\u30FCフ
		{
			get
			{
				return tex.Race[50];
			}
		}

		public static string ハ\u30FCピ\u30FC
		{
			get
			{
				return tex.Race[51];
			}
		}

		public static string バイコ\u30FCン
		{
			get
			{
				return tex.Race[52];
			}
		}

		public static string バジリスク
		{
			get
			{
				return tex.Race[53];
			}
		}

		public static string ハルピュイア
		{
			get
			{
				return tex.Race[54];
			}
		}

		public static string パンテ\u30FCラケンタウレ
		{
			get
			{
				return tex.Race[55];
			}
		}

		public static string ヒッポグリフ
		{
			get
			{
				return tex.Race[56];
			}
		}

		public static string ヒッポケンタウレ
		{
			get
			{
				return tex.Race[57];
			}
		}

		public static string ヒュ\u30FCマン
		{
			get
			{
				return tex.Race[58];
			}
		}

		public static string ヒュドラ
		{
			get
			{
				return tex.Race[59];
			}
		}

		public static string フェアリ\u30FC
		{
			get
			{
				return tex.Race[60];
			}
		}

		public static string フェニックス
		{
			get
			{
				return tex.Race[61];
			}
		}

		public static string ブケンタウレ
		{
			get
			{
				return tex.Race[62];
			}
		}

		public static string ペガサス
		{
			get
			{
				return tex.Race[63];
			}
		}

		public static string マ\u30FCメイド
		{
			get
			{
				return tex.Race[64];
			}
		}

		public static string ミックス
		{
			get
			{
				return tex.Race[65];
			}
		}

		public static string ミノタウロス
		{
			get
			{
				return tex.Race[66];
			}
		}

		public static string ムカデジョウロウ
		{
			get
			{
				return tex.Race[67];
			}
		}

		public static string モノケロス
		{
			get
			{
				return tex.Race[68];
			}
		}

		public static string ユニコ\u30FCン
		{
			get
			{
				return tex.Race[69];
			}
		}

		public static string ラミア
		{
			get
			{
				return tex.Race[70];
			}
		}

		public static string リザ\u30FCドマン
		{
			get
			{
				return tex.Race[71];
			}
		}

		public static string リュウ
		{
			get
			{
				return tex.Race[72];
			}
		}

		public static string リリン
		{
			get
			{
				return tex.Race[73];
			}
		}

		public static string レオントケンタウレ
		{
			get
			{
				return tex.Race[74];
			}
		}

		public static string ワ\u30FCム
		{
			get
			{
				return tex.Race[75];
			}
		}

		public static string ワイバ\u30FCン
		{
			get
			{
				return tex.Race[76];
			}
		}

		public static string 調教済
		{
			get
			{
				return tex.Attr[0];
			}
		}

		public static string 無毛
		{
			get
			{
				return tex.Attr[1];
			}
		}

		public static string 処女
		{
			get
			{
				return tex.Attr[2];
			}
		}

		public static string 発情
		{
			get
			{
				return tex.Attr[3];
			}
		}

		public static string 妊娠
		{
			get
			{
				return tex.Attr[4];
			}
		}

		public static string 強靭
		{
			get
			{
				return tex.Attr[5];
			}
		}

		public static string 傷物
		{
			get
			{
				return tex.Attr[6];
			}
		}

		public static string オッドアイ
		{
			get
			{
				return tex.Attr[7];
			}
		}

		public static string ルチノ\u30FC
		{
			get
			{
				return tex.Attr[8];
			}
		}

		public static string メラニス
		{
			get
			{
				return tex.Attr[9];
			}
		}

		public static string アルビノ
		{
			get
			{
				return tex.Attr[10];
			}
		}

		public static string はい
		{
			get
			{
				return tex.Comm[0];
			}
		}

		public static string いいえ
		{
			get
			{
				return tex.Comm[1];
			}
		}

		public static string 所持金
		{
			get
			{
				return tex.Comm[2];
			}
		}

		public static string 借金
		{
			get
			{
				return tex.Comm[3];
			}
		}

		public static string 日目
		{
			get
			{
				return tex.Comm[4];
			}
		}

		public static string 朝
		{
			get
			{
				return tex.Comm[5];
			}
		}

		public static string 昼
		{
			get
			{
				return tex.Comm[6];
			}
		}

		public static string 夜
		{
			get
			{
				return tex.Comm[7];
			}
		}

		public static string 合計売却額
		{
			get
			{
				return tex.Comm[8];
			}
		}

		public static string 種族
		{
			get
			{
				return tex.Comm[9];
			}
		}

		public static string 価格
		{
			get
			{
				return tex.Comm[10];
			}
		}

		public static string 体力
		{
			get
			{
				return tex.Comm[11];
			}
		}

		public static string 感度
		{
			get
			{
				return tex.Comm[12];
			}
		}

		public static string 興奮
		{
			get
			{
				return tex.Comm[13];
			}
		}

		public static string 潤滑
		{
			get
			{
				return tex.Comm[14];
			}
		}

		public static string 緊張
		{
			get
			{
				return tex.Comm[15];
			}
		}

		public static string 羞恥
		{
			get
			{
				return tex.Comm[16];
			}
		}

		public static string 抵抗値
		{
			get
			{
				return tex.Comm[17];
			}
		}

		public static string 魔力濃度
		{
			get
			{
				return tex.Comm[18];
			}
		}

		public static string 欲望度
		{
			get
			{
				return tex.Comm[19];
			}
		}

		public static string 情愛度
		{
			get
			{
				return tex.Comm[20];
			}
		}

		public static string 卑屈度
		{
			get
			{
				return tex.Comm[21];
			}
		}

		public static string 技巧度
		{
			get
			{
				return tex.Comm[22];
			}
		}

		public static string 調教力
		{
			get
			{
				return tex.Comm[23];
			}
		}

		public static string 部位感度合計
		{
			get
			{
				return tex.Comm[24];
			}
		}

		public static string バストサイズ
		{
			get
			{
				return tex.Comm[25];
			}
		}

		public static string 一般労働
		{
			get
			{
				return tex.Comm[26];
			}
		}

		public static string 娼婦労働
		{
			get
			{
				return tex.Comm[27];
			}
		}

		public static string 需給
		{
			get
			{
				return tex.Comm[28];
			}
		}

		public static string 売値
		{
			get
			{
				return tex.Comm[29];
			}
		}

		public static string 買値
		{
			get
			{
				return tex.Comm[30];
			}
		}

		public static string 利益
		{
			get
			{
				return tex.Comm[31];
			}
		}

		public static string 戻る
		{
			get
			{
				return tex.Comm[32];
			}
		}

		public static string 収容番号
		{
			get
			{
				return tex.Comm[33];
			}
		}

		public static string 所持金が足りません
		{
			get
			{
				return tex.Comm[34];
			}
		}

		public static string タイトル画面に戻りますか
		{
			get
			{
				return tex.Comm[35];
			}
		}

		public static string セ\u30FCブ中です
		{
			get
			{
				return tex.Comm[36];
			}
		}

		public static string セ\u30FCブしました
		{
			get
			{
				return tex.Comm[37];
			}
		}

		public static string ロ\u30FCド中です
		{
			get
			{
				return tex.Comm[38];
			}
		}

		public static string ロ\u30FCドしました
		{
			get
			{
				return tex.Comm[39];
			}
		}

		public static string しばらくお待ちください
		{
			get
			{
				return tex.Comm[40];
			}
		}

		public static string が妊娠しました
		{
			get
			{
				return tex.Comm[41];
			}
		}

		public static string 労働が解除されます
		{
			get
			{
				return tex.Comm[42];
			}
		}

		public static string が出産しました
		{
			get
			{
				return tex.Comm[43];
			}
		}

		public static string が増殖しました
		{
			get
			{
				return tex.Comm[44];
			}
		}

		public static string 収容できないので子は売却されます
		{
			get
			{
				return tex.Comm[45];
			}
		}

		public static string 子を奴隷として収容します
		{
			get
			{
				return tex.Comm[46];
			}
		}

		public static string プレイヤ\u30FCの遺伝情報を設定します
		{
			get
			{
				return tex.Comm[47];
			}
		}

		public static string 労働利益
		{
			get
			{
				return tex.Comm[48];
			}
		}

		public static string 利子
		{
			get
			{
				return tex.Comm[49];
			}
		}

		public static string 完了
		{
			get
			{
				return tex.Comm[50];
			}
		}

		public static string 肌の色
		{
			get
			{
				return tex.Comm[51];
			}
		}

		public static string 髪の色
		{
			get
			{
				return tex.Comm[52];
			}
		}

		public static string 瞳の色
		{
			get
			{
				return tex.Comm[53];
			}
		}

		public static string 体格
		{
			get
			{
				return tex.Comm[54];
			}
		}

		public static string プレイヤ\u30FCの遺伝情報を設定してください
		{
			get
			{
				return tex.Comm[55];
			}
		}

		public static string 後から変更できます
		{
			get
			{
				return tex.Comm[56];
			}
		}

		public static string スレイブマトリクス
		{
			get
			{
				return tex.Comm[57];
			}
		}

		public static string 事務所
		{
			get
			{
				return tex.Base[0];
			}
		}

		public static string 調教
		{
			get
			{
				return tex.Base[1];
			}
		}

		public static string 対象
		{
			get
			{
				return tex.Base[2];
			}
		}

		public static string 休む
		{
			get
			{
				return tex.Base[3];
			}
		}

		public static string 眠る
		{
			get
			{
				return tex.Base[4];
			}
		}

		public static string 祝福
		{
			get
			{
				return tex.Base[5];
			}
		}

		public static string チェンジ
		{
			get
			{
				return tex.Base[6];
			}
		}

		public static string 他の奴隷がいません
		{
			get
			{
				return tex.Base[7];
			}
		}

		public static string 奴隷をランダムに選択します
		{
			get
			{
				return tex.Base[8];
			}
		}

		public static string 点6
		{
			get
			{
				return tex.Trai[0];
			}
		}

		public static string の調教が完了しました
		{
			get
			{
				return tex.Trai[1];
			}
		}

		public static string から祝福を受けました
		{
			get
			{
				return tex.Trai[2];
			}
		}

		public static string 選択
		{
			get
			{
				return tex.Trai[3];
			}
		}

		public static string 持つ
		{
			get
			{
				return tex.Trai[4];
			}
		}

		public static string 拘束状態を切換えます
		{
			get
			{
				return tex.Trai[5];
			}
		}

		public static string 目隠状態を切換えます
		{
			get
			{
				return tex.Trai[6];
			}
		}

		public static string 口枷状態を切換えます
		{
			get
			{
				return tex.Trai[7];
			}
		}

		public static string 断面表示を切換えます
		{
			get
			{
				return tex.Trai[8];
			}
		}

		public static string 発情状態になります
		{
			get
			{
				return tex.Trai[9];
			}
		}

		public static string 撮影しました
		{
			get
			{
				return tex.Trai[10];
			}
		}

		public static string 写真はPhotoフォルダに保存されます
		{
			get
			{
				return tex.Trai[11];
			}
		}

		public static string 撮影を行います
		{
			get
			{
				return tex.Trai[12];
			}
		}

		public static string 調教終了
		{
			get
			{
				return tex.Trai[13];
			}
		}

		public static string 拘束
		{
			get
			{
				return tex.Trai[14];
			}
		}

		public static string 目隠
		{
			get
			{
				return tex.Trai[15];
			}
		}

		public static string 口枷
		{
			get
			{
				return tex.Trai[16];
			}
		}

		public static string 断面
		{
			get
			{
				return tex.Trai[17];
			}
		}

		public static string 媚薬
		{
			get
			{
				return tex.Trai[18];
			}
		}

		public static string 媚薬を打ち込んだ
		{
			get
			{
				return tex.Trai[19];
			}
		}

		public static string 撮影
		{
			get
			{
				return tex.Trai[20];
			}
		}

		public static string 愛想が悪い
		{
			get
			{
				return tex.Trai[21];
			}
		}

		public static string ダブルピ\u30FCスさせますか
		{
			get
			{
				return tex.Trai[22];
			}
		}

		public static string 停止
		{
			get
			{
				return tex.Trai[23];
			}
		}

		public static string 作動
		{
			get
			{
				return tex.Trai[24];
			}
		}

		public static string 強さL
		{
			get
			{
				return tex.Trai[25];
			}
		}

		public static string 放す
		{
			get
			{
				return tex.Trai[26];
			}
		}

		public static string 引抜く
		{
			get
			{
				return tex.Trai[27];
			}
		}

		public static string 離す
		{
			get
			{
				return tex.Trai[28];
			}
		}

		public static string 押付け
		{
			get
			{
				return tex.Trai[29];
			}
		}

		public static string 挿入
		{
			get
			{
				return tex.Trai[30];
			}
		}

		public static string 口腔
		{
			get
			{
				return tex.Trai[31];
			}
		}

		public static string 肛門
		{
			get
			{
				return tex.Trai[32];
			}
		}

		public static string 膣腔
		{
			get
			{
				return tex.Trai[33];
			}
		}

		public static string 出糸
		{
			get
			{
				return tex.Trai[34];
			}
		}

		public static string やめる
		{
			get
			{
				return tex.Trai[35];
			}
		}

		public static string 擽り
		{
			get
			{
				return tex.Trai[36];
			}
		}

		public static string 剃る
		{
			get
			{
				return tex.Trai[37];
			}
		}

		public static string 当てる
		{
			get
			{
				return tex.Trai[38];
			}
		}

		public static string 左打
		{
			get
			{
				return tex.Trai[39];
			}
		}

		public static string 右打
		{
			get
			{
				return tex.Trai[40];
			}
		}

		public static string 乳首
		{
			get
			{
				return tex.Trai[41];
			}
		}

		public static string 摘む
		{
			get
			{
				return tex.Trai[42];
			}
		}

		public static string 繰る
		{
			get
			{
				return tex.Trai[43];
			}
		}

		public static string 動かす
		{
			get
			{
				return tex.Trai[44];
			}
		}

		public static string 乳房
		{
			get
			{
				return tex.Trai[45];
			}
		}

		public static string 掴む
		{
			get
			{
				return tex.Trai[46];
			}
		}

		public static string 捏ねる
		{
			get
			{
				return tex.Trai[47];
			}
		}

		public static string 陰核
		{
			get
			{
				return tex.Trai[48];
			}
		}

		public static string 触れる
		{
			get
			{
				return tex.Trai[49];
			}
		}

		public static string 陰唇
		{
			get
			{
				return tex.Trai[50];
			}
		}

		public static string 広げる
		{
			get
			{
				return tex.Trai[51];
			}
		}

		public static string 撫でる
		{
			get
			{
				return tex.Trai[52];
			}
		}

		public static string マウス切替
		{
			get
			{
				return tex.Trai[53];
			}
		}

		public static string ペニス切替
		{
			get
			{
				return tex.Trai[54];
			}
		}

		public static string 吸引
		{
			get
			{
				return tex.Trai[55];
			}
		}

		public static string 舐る
		{
			get
			{
				return tex.Trai[56];
			}
		}

		public static string 開放
		{
			get
			{
				return tex.Trai[57];
			}
		}

		public static string 擦る
		{
			get
			{
				return tex.Trai[58];
			}
		}

		public static string 挟ませる
		{
			get
			{
				return tex.Trai[59];
			}
		}

		public static string 挟む
		{
			get
			{
				return tex.Trai[60];
			}
		}

		public static string やめさせる
		{
			get
			{
				return tex.Trai[61];
			}
		}

		public static string 手コキ
		{
			get
			{
				return tex.Trai[62];
			}
		}

		public static string 足コキ
		{
			get
			{
				return tex.Trai[63];
			}
		}

		public static string 扱く
		{
			get
			{
				return tex.Trai[64];
			}
		}

		public static string ハンド切替
		{
			get
			{
				return tex.Trai[65];
			}
		}

		public static string 外す
		{
			get
			{
				return tex.Trai[66];
			}
		}

		public static string 装着
		{
			get
			{
				return tex.Trai[67];
			}
		}

		public static string 体力が限界です調教を終了します
		{
			get
			{
				return tex.Trai[68];
			}
		}

		public static string 点6ハ\u30FCト
		{
			get
			{
				return tex.Trai[69];
			}
		}

		public static string 子
		{
			get
			{
				return tex.Targ[0];
			}
		}

		public static string 親形質1
		{
			get
			{
				return tex.Targ[1];
			}
		}

		public static string 親形質2
		{
			get
			{
				return tex.Targ[2];
			}
		}

		public static string 保守
		{
			get
			{
				return tex.Targ[3];
			}
		}

		public static string 奴隷を保守対象に設定しました
		{
			get
			{
				return tex.Targ[4];
			}
		}

		public static string 奴隷の保守設定を解除しました
		{
			get
			{
				return tex.Targ[5];
			}
		}

		public static string 奴隷を一般労働に設定しました
		{
			get
			{
				return tex.Targ[6];
			}
		}

		public static string 奴隷の一般労働を解除しました
		{
			get
			{
				return tex.Targ[7];
			}
		}

		public static string 奴隷を娼婦労働に設定しました
		{
			get
			{
				return tex.Targ[8];
			}
		}

		public static string 奴隷の娼婦労働を解除しました
		{
			get
			{
				return tex.Targ[9];
			}
		}

		public static string 全一般
		{
			get
			{
				return tex.Targ[10];
			}
		}

		public static string 労働可能な全ての奴隷に一般労働を設定しました
		{
			get
			{
				return tex.Targ[11];
			}
		}

		public static string 全娼婦
		{
			get
			{
				return tex.Targ[12];
			}
		}

		public static string 労働可能な全ての奴隷に娼婦労働を設定しました
		{
			get
			{
				return tex.Targ[13];
			}
		}

		public static string 全解除
		{
			get
			{
				return tex.Targ[14];
			}
		}

		public static string 労働中の全ての奴隷の労働を解除しました
		{
			get
			{
				return tex.Targ[15];
			}
		}

		public static string 売却
		{
			get
			{
				return tex.Targ[16];
			}
		}

		public static string 売却しますか
		{
			get
			{
				return tex.Targ[17];
			}
		}

		public static string を売却しました
		{
			get
			{
				return tex.Targ[18];
			}
		}

		public static string 売却をキャンセルしました
		{
			get
			{
				return tex.Targ[19];
			}
		}

		public static string 全売却
		{
			get
			{
				return tex.Targ[20];
			}
		}

		public static string 保守以外の全ての奴隷を売却しますか
		{
			get
			{
				return tex.Targ[21];
			}
		}

		public static string 保守以外の全ての奴隷を売却しました
		{
			get
			{
				return tex.Targ[22];
			}
		}

		public static string 全売却をキャンセルしました
		{
			get
			{
				return tex.Targ[23];
			}
		}

		public static string 胸施術
		{
			get
			{
				return tex.Targ[24];
			}
		}

		public static string 胸の甲殻を切除しました
		{
			get
			{
				return tex.Targ[25];
			}
		}

		public static string 股施術
		{
			get
			{
				return tex.Targ[26];
			}
		}

		public static string 股の
		{
			get
			{
				return tex.Targ[27];
			}
		}

		public static string 鱗
		{
			get
			{
				return tex.Targ[28];
			}
		}

		public static string 甲殻
		{
			get
			{
				return tex.Targ[29];
			}
		}

		public static string を切除しました
		{
			get
			{
				return tex.Targ[30];
			}
		}

		public static string 淫紋
		{
			get
			{
				return tex.Targ[31];
			}
		}

		public static string 淫紋を刻みました
		{
			get
			{
				return tex.Targ[32];
			}
		}

		public static string 衣装
		{
			get
			{
				return tex.Targ[33];
			}
		}

		public static string 衣装を変更しました
		{
			get
			{
				return tex.Targ[34];
			}
		}

		public static string 胸の甲殻を切除します
		{
			get
			{
				return tex.Targ[35];
			}
		}

		public static string を切除します
		{
			get
			{
				return tex.Targ[36];
			}
		}

		public static string 淫紋を刻みます
		{
			get
			{
				return tex.Targ[37];
			}
		}

		public static string 衣装を変更します
		{
			get
			{
				return tex.Targ[38];
			}
		}

		public static string 奴隷の保守設定を切り替えます
		{
			get
			{
				return tex.Targ[39];
			}
		}

		public static string 奴隷の一般労働設定を切り替えます
		{
			get
			{
				return tex.Targ[40];
			}
		}

		public static string 奴隷の娼婦労働設定を切り替えます
		{
			get
			{
				return tex.Targ[41];
			}
		}

		public static string 奴隷を売却します
		{
			get
			{
				return tex.Targ[42];
			}
		}

		public static string 労働可能な全ての奴隷を働かせます
		{
			get
			{
				return tex.Targ[43];
			}
		}

		public static string 労働可能な全ての奴隷を娼婦として働かせます
		{
			get
			{
				return tex.Targ[44];
			}
		}

		public static string 全ての奴隷の労働を解除します
		{
			get
			{
				return tex.Targ[45];
			}
		}

		public static string 保守以外の全ての奴隷を売却します
		{
			get
			{
				return tex.Targ[46];
			}
		}

		public static string 対象が設定されていません
		{
			get
			{
				return tex.Targ[47];
			}
		}

		public static string 祝福解除
		{
			get
			{
				return tex.Bles[0];
			}
		}

		public static string 祝福を解除しました
		{
			get
			{
				return tex.Bles[1];
			}
		}

		public static string から祝福を受けています
		{
			get
			{
				return tex.Bles[2];
			}
		}

		public static string 祝福されていません
		{
			get
			{
				return tex.Bles[3];
			}
		}

		public static string 購入
		{
			get
			{
				return tex.Offi[0];
			}
		}

		public static string いらっしゃい待っていたわ
		{
			get
			{
				return tex.Offi[1];
			}
		}

		public static string 今日はどうしたの
		{
			get
			{
				return tex.Offi[2];
			}
		}

		public static string 借
		{
			get
			{
				return tex.Bebt[0];
			}
		}

		public static string 借金可能額以上は無視されます
		{
			get
			{
				return tex.Bebt[1];
			}
		}

		public static string 今日はこれ以上借りることが出来ません
		{
			get
			{
				return tex.Bebt[2];
			}
		}

		public static string 返
		{
			get
			{
				return tex.Bebt[3];
			}
		}

		public static string 返済可能額以上は無視されます
		{
			get
			{
				return tex.Bebt[4];
			}
		}

		public static string 所持金がありません
		{
			get
			{
				return tex.Bebt[5];
			}
		}

		public static string 今日借りれる金額はあと
		{
			get
			{
				return tex.Bebt[6];
			}
		}

		public static string よ
		{
			get
			{
				return tex.Bebt[7];
			}
		}

		public static string 奴隷
		{
			get
			{
				return tex.Slav[0];
			}
		}

		public static string 道具
		{
			get
			{
				return tex.Slav[1];
			}
		}

		public static string ランダム
		{
			get
			{
				return tex.Slav[2];
			}
		}

		public static string 鳥系
		{
			get
			{
				return tex.Slav[3];
			}
		}

		public static string 蛇系
		{
			get
			{
				return tex.Slav[4];
			}
		}

		public static string 獣系
		{
			get
			{
				return tex.Slav[5];
			}
		}

		public static string 水系
		{
			get
			{
				return tex.Slav[6];
			}
		}

		public static string 虫系
		{
			get
			{
				return tex.Slav[7];
			}
		}

		public static string 人型
		{
			get
			{
				return tex.Slav[8];
			}
		}

		public static string 幻獣
		{
			get
			{
				return tex.Slav[9];
			}
		}

		public static string 魔獣
		{
			get
			{
				return tex.Slav[10];
			}
		}

		public static string 竜系
		{
			get
			{
				return tex.Slav[11];
			}
		}

		public static string 売り切れです
		{
			get
			{
				return tex.Slav[12];
			}
		}

		public static string これ以上奴隷を収容できません
		{
			get
			{
				return tex.Slav[13];
			}
		}

		public static string 買える物は何も無い
		{
			get
			{
				return tex.Tool[0];
			}
		}

		public static string ﾃ\uFF9Eｨﾙﾄ\uFF9Eﾊ\uFF9Eｲﾌ\uFF9E
		{
			get
			{
				return tex.Tool[1];
			}
		}

		public static string ﾉ\uFF70ﾏﾙﾊ\uFF9Eｲﾌ\uFF9E
		{
			get
			{
				return tex.Tool[2];
			}
		}

		public static string ﾄ\uFF9Eﾘﾙﾊ\uFF9Eｲﾌ\uFF9E
		{
			get
			{
				return tex.Tool[3];
			}
		}

		public static string ﾃ\uFF9Eﾝﾏﾊ\uFF9Eｲﾌ\uFF9E
		{
			get
			{
				return tex.Tool[4];
			}
		}

		public static string ｱﾅﾙﾊ\uFF9Eｲﾌ\uFF9E
		{
			get
			{
				return tex.Tool[5];
			}
		}

		public static string 調教鞭
		{
			get
			{
				return tex.Tool[6];
			}
		}

		public static string 羽根箒
		{
			get
			{
				return tex.Tool[7];
			}
		}

		public static string T字剃刀
		{
			get
			{
				return tex.Tool[8];
			}
		}

		public static string 振動ｷｬｯﾌ\uFF9F
		{
			get
			{
				return tex.Tool[9];
			}
		}

		public static string ﾋ\uFF9Fﾝｸﾛ\uFF70ﾀ
		{
			get
			{
				return tex.Tool[10];
			}
		}

		public static string ｱﾅﾙﾊ\uFF9F\uFF70ﾙ
		{
			get
			{
				return tex.Tool[11];
			}
		}

		public static string 目隠帯
		{
			get
			{
				return tex.Tool[12];
			}
		}

		public static string 玉口枷
		{
			get
			{
				return tex.Tool[13];
			}
		}

		public static string カメラ
		{
			get
			{
				return tex.Tool[14];
			}
		}

		public static string ﾌﾛｱ増設
		{
			get
			{
				return tex.Tool[15];
			}
		}

		public static string ふざけた値段だ
		{
			get
			{
				return tex.Tool[16];
			}
		}

		public static string ペニスを模したバイブ
		{
			get
			{
				return tex.Tool[17];
			}
		}

		public static string 刺激は控えめ
		{
			get
			{
				return tex.Tool[18];
			}
		}

		public static string 一般的なバイブ
		{
			get
			{
				return tex.Tool[19];
			}
		}

		public static string ディルドバイブより刺激が強い
		{
			get
			{
				return tex.Tool[20];
			}
		}

		public static string 振動と回転の2つの刺激をもたらすバイブ
		{
			get
			{
				return tex.Tool[21];
			}
		}

		public static string 強力な振動のバイブ
		{
			get
			{
				return tex.Tool[22];
			}
		}

		public static string 刺激が強い
		{
			get
			{
				return tex.Tool[23];
			}
		}

		public static string アナルの調教に適したバイブ
		{
			get
			{
				return tex.Tool[24];
			}
		}

		public static string 痛みを与えるための道具
		{
			get
			{
				return tex.Tool[25];
			}
		}

		public static string 緊張を解きほぐすために利用する
		{
			get
			{
				return tex.Tool[26];
			}
		}

		public static string 陰毛を剃ることが出来る
		{
			get
			{
				return tex.Tool[27];
			}
		}

		public static string 吸着振動するキャップ
		{
			get
			{
				return tex.Tool[28];
			}
		}

		public static string 刺激の弱いバイブの一種
		{
			get
			{
				return tex.Tool[29];
			}
		}

		public static string アナルの調教に適した道具
		{
			get
			{
				return tex.Tool[30];
			}
		}

		public static string 奴隷の視界を遮るための道具
		{
			get
			{
				return tex.Tool[31];
			}
		}

		public static string 奴隷の口をふさぐための道具
		{
			get
			{
				return tex.Tool[32];
			}
		}

		public static string 写真を撮影することが出来る
		{
			get
			{
				return tex.Tool[33];
			}
		}

		public static string フロアを増設し収容できる奴隷の数を増やす
		{
			get
			{
				return tex.Tool[34];
			}
		}

		public static string 点12
		{
			get
			{
				return tex.OP0[0];
			}
		}

		public static string 点9
		{
			get
			{
				return tex.OP0[1];
			}
		}

		public static string 点3
		{
			get
			{
				return tex.OP0[2];
			}
		}

		public static string 闇が揺れる
		{
			get
			{
				return tex.OP0[3];
			}
		}

		public static string 身体が現る
		{
			get
			{
				return tex.OP0[4];
			}
		}

		public static string 意識が宿る
		{
			get
			{
				return tex.OP0[5];
			}
		}

		public static string お目覚めかしら
		{
			get
			{
				return tex.OP1[0];
			}
		}

		public static string 話の途中で眠ってしまうんですもの
		{
			get
			{
				return tex.OP1[1];
			}
		}

		public static string よっぽど疲れていたのね
		{
			get
			{
				return tex.OP1[2];
			}
		}

		public static string 誰って顔をしているわね
		{
			get
			{
				return tex.OP1[3];
			}
		}

		public static string うふふ良いわ
		{
			get
			{
				return tex.OP1[4];
			}
		}

		public static string もう一度自己紹介をしてあげる
		{
			get
			{
				return tex.OP1[5];
			}
		}

		public static string 私の名前はヴィオランテ
		{
			get
			{
				return tex.OP1[6];
			}
		}

		public static string ここではヴィオラと呼ばれているわ
		{
			get
			{
				return tex.OP1[7];
			}
		}

		public static string それでは話の続きをしましょうか
		{
			get
			{
				return tex.OP1[8];
			}
		}

		public static string 今あなたには全部で
		{
			get
			{
				return tex.OP1[9];
			}
		}

		public static string の借金が課せられているわ
		{
			get
			{
				return tex.OP1[10];
			}
		}

		public static string でも心配しないで
		{
			get
			{
				return tex.OP1[11];
			}
		}

		public static string そんなあなたにぴったりなお仕事を紹介してあげる
		{
			get
			{
				return tex.OP1[12];
			}
		}

		public static string とっても儲かる素敵なお仕事をね
		{
			get
			{
				return tex.OP1[13];
			}
		}

		public static string 点6ハテナ
		{
			get
			{
				return tex.OP1[14];
			}
		}

		public static string 光が射す
		{
			get
			{
				return tex.OP1[15];
			}
		}

		public static string 目が眩む
		{
			get
			{
				return tex.OP1[16];
			}
		}

		public static string 見慣れぬ女が立っている
		{
			get
			{
				return tex.OP1[17];
			}
		}

		public static string 良い香りがする
		{
			get
			{
				return tex.OP1[18];
			}
		}

		public static string 空気はぬるい
		{
			get
			{
				return tex.OP1[19];
			}
		}

		public static string 女は語る
		{
			get
			{
				return tex.OP1[20];
			}
		}

		public static string 女は続ける
		{
			get
			{
				return tex.OP1[21];
			}
		}

		public static string 女は名乗る
		{
			get
			{
				return tex.OP1[22];
			}
		}

		public static string 違和感を覚える
		{
			get
			{
				return tex.OP1[23];
			}
		}

		public static string 知らない世界だ
		{
			get
			{
				return tex.OP1[24];
			}
		}

		public static string なんて日だ
		{
			get
			{
				return tex.OP1[25];
			}
		}

		public static string 話が進む
		{
			get
			{
				return tex.OP1[26];
			}
		}

		public static string 事は運ぶ
		{
			get
			{
				return tex.OP1[27];
			}
		}

		public static string 安い額ではない
		{
			get
			{
				return tex.OP1[28];
			}
		}

		public static string うふふそうよね
		{
			get
			{
				return tex.OP1[29];
			}
		}

		public static string 身に覚えはない
		{
			get
			{
				return tex.OP1[30];
			}
		}

		public static string 関係ないわここに書いてあるもの
		{
			get
			{
				return tex.OP1[31];
			}
		}

		public static string ほらねそうでしょう
		{
			get
			{
				return tex.OP1[32];
			}
		}

		public static string ここは地下牢よ
		{
			get
			{
				return tex.Desc[0];
			}
		}

		public static string あなたにはこの場所で調教師として働いてもらうわ
		{
			get
			{
				return tex.Desc[1];
			}
		}

		public static string 仕事の流れを説明するわね
		{
			get
			{
				return tex.Desc[2];
			}
		}

		public static string まず事務所で奴隷を仕入れて頂戴
		{
			get
			{
				return tex.Desc[3];
			}
		}

		public static string 仕入れたら性奴として使えるようにあなたが躾けるの
		{
			get
			{
				return tex.Desc[4];
			}
		}

		public static string 躾けた分だけ上乗せされた値段で売れるようになるわ
		{
			get
			{
				return tex.Desc[5];
			}
		}

		public static string それと従順になった奴隷に働いてもらうのもいいわね
		{
			get
			{
				return tex.Desc[6];
			}
		}

		public static string あと奴隷の転売という手もあるわ
		{
			get
			{
				return tex.Desc[7];
			}
		}

		public static string まぁこんなところね簡単でしょう
		{
			get
			{
				return tex.Desc[8];
			}
		}

		public static string それでは頑張って頂戴
		{
			get
			{
				return tex.Desc[9];
			}
		}

		public static string 期待しているわ
		{
			get
			{
				return tex.Desc[10];
			}
		}

		public static string 冷めた空気が舞い上がる
		{
			get
			{
				return tex.Desc[11];
			}
		}

		public static string ヴィオラは語る
		{
			get
			{
				return tex.Desc[12];
			}
		}

		public static string 仕事が決まる
		{
			get
			{
				return tex.Desc[13];
			}
		}

		public static string 説明が始まる
		{
			get
			{
				return tex.Desc[14];
			}
		}

		public static string ヴィオラは続ける
		{
			get
			{
				return tex.Desc[15];
			}
		}

		public static string 説明が続く
		{
			get
			{
				return tex.Desc[16];
			}
		}

		public static string 説明は続く
		{
			get
			{
				return tex.Desc[17];
			}
		}

		public static string 説明が終わる
		{
			get
			{
				return tex.Desc[18];
			}
		}

		public static string 奴隷母体の名の下に
		{
			get
			{
				return tex.Desc[19];
			}
		}

		public static string 物語は動き出す
		{
			get
			{
				return tex.Desc[20];
			}
		}

		public static string どうかしたの
		{
			get
			{
				return tex.Firs[0];
			}
		}

		public static string お金がなければ借りればいいじゃない
		{
			get
			{
				return tex.Firs[1];
			}
		}

		public static string 借金と返済は事務所つまりここで出来るわ
		{
			get
			{
				return tex.Firs[2];
			}
		}

		public static string 借金には1日
		{
			get
			{
				return tex.Firs[3];
			}
		}

		public static string の利子がつくわよ
		{
			get
			{
				return tex.Firs[4];
			}
		}

		public static string 良心的よねうふふ
		{
			get
			{
				return tex.Firs[5];
			}
		}

		public static string そうそう言い忘れていたけどあなたは調教師として必要な拘束術が使えるようになっているはずよ
		{
			get
			{
				return tex.Firs[6];
			}
		}

		public static string 私からは以上よ
		{
			get
			{
				return tex.Firs[7];
			}
		}

		public static string 仕事に戻ると良いわ
		{
			get
			{
				return tex.Firs[8];
			}
		}

		public static string ヴィオラは佇む
		{
			get
			{
				return tex.Firs[9];
			}
		}

		public static string ヴィオラは尋ねる
		{
			get
			{
				return tex.Firs[10];
			}
		}

		public static string ヴィオラは返す
		{
			get
			{
				return tex.Firs[11];
			}
		}

		public static string 話は続く
		{
			get
			{
				return tex.Firs[12];
			}
		}

		public static string 危険な女だ
		{
			get
			{
				return tex.Firs[13];
			}
		}

		public static string 話が終わる
		{
			get
			{
				return tex.Firs[14];
			}
		}

		public static string 話は終わる
		{
			get
			{
				return tex.Firs[15];
			}
		}

		public static string あなたは答える
		{
			get
			{
				return tex.Firs[16];
			}
		}

		public static string 嘘おっしゃい無いのは分かっているわ
		{
			get
			{
				return tex.Firs[17];
			}
		}

		public static string 金が無い
		{
			get
			{
				return tex.Firs[18];
			}
		}

		public static string エクス2
		{
			get
			{
				return tex.Firs[19];
			}
		}

		public static string あなたの鎖は弾け飛ぶ
		{
			get
			{
				return tex.Firs[20];
			}
		}

		public static string あらあら今ので利子が上がってしまったわうふふ
		{
			get
			{
				return tex.Firs[21];
			}
		}

		public static string っ点3
		{
			get
			{
				return tex.Firs[22];
			}
		}

		public static string エクス1
		{
			get
			{
				return tex.Firs[23];
			}
		}

		public static string 鋼の鎖がヴィオラを縛る
		{
			get
			{
				return tex.Firs[24];
			}
		}

		public static string 点3うふふ
		{
			get
			{
				return tex.Firs[25];
			}
		}

		public static string 慎重なのは良いことよ
		{
			get
			{
				return tex.Firs[26];
			}
		}

		public static string ヴィオラは微笑む
		{
			get
			{
				return tex.Firs[27];
			}
		}

		public static string ヴィオラで試す
		{
			get
			{
				return tex.Firs[28];
			}
		}

		public static string 仕事には慣れたかしら
		{
			get
			{
				return tex.Repa1[0];
			}
		}

		public static string あなたが頑張っているおかげで奴隷の仕入元のハンタ\u30FCと話がまとまったの
		{
			get
			{
				return tex.Repa1[1];
			}
		}

		public static string 今後もこの調子で頑張って頂戴
		{
			get
			{
				return tex.Repa1[2];
			}
		}

		public static string ヴィオラが尋ねる
		{
			get
			{
				return tex.Repa1[3];
			}
		}

		public static string うふふ
		{
			get
			{
				return tex.Repa2[0];
			}
		}

		public static string なかなか順調のようね
		{
			get
			{
				return tex.Repa2[1];
			}
		}

		public static string そろそろ奴隷の身嗜みを考えてみてもいいんじゃないかしら
		{
			get
			{
				return tex.Repa2[2];
			}
		}

		public static string 身嗜みに手を加えられるように手配しておくわね
		{
			get
			{
				return tex.Repa2[3];
			}
		}

		public static string ヴィオラが語る
		{
			get
			{
				return tex.Repa2[4];
			}
		}

		public static string まさか本当に完済してくれるとは思わなかったわ
		{
			get
			{
				return tex.Repa3[0];
			}
		}

		public static string もうあなたがここに縛られている理由は何もないわね
		{
			get
			{
				return tex.Repa3[1];
			}
		}

		public static string だから好きになさい
		{
			get
			{
				return tex.Repa3[2];
			}
		}

		public static string そんなこと言わずにもっとゆっくりしていくといいわ
		{
			get
			{
				return tex.Repa3[3];
			}
		}

		public static string 別にあなたを帰すときのことを考えてなかったとかそういうことではないのよ
		{
			get
			{
				return tex.Repa3[4];
			}
		}

		public static string とにかくご苦労様
		{
			get
			{
				return tex.Repa3[5];
			}
		}

		public static string そしてありがとう調教師
		{
			get
			{
				return tex.Repa3[6];
			}
		}

		public static string あなたを手放すなんてありえないわ
		{
			get
			{
				return tex.Repa3[7];
			}
		}

		public static string なんてったって
		{
			get
			{
				return tex.Repa3[8];
			}
		}

		public static string もの稼ぎ手ですもの
		{
			get
			{
				return tex.Repa3[9];
			}
		}

		public static string お勤めご苦労様
		{
			get
			{
				return tex.Repa3[10];
			}
		}

		public static string あなたは自由だ
		{
			get
			{
				return tex.Repa3[11];
			}
		}

		public static string ヴィオラは黙る
		{
			get
			{
				return tex.Repa3[12];
			}
		}

		public static string ヴィオラは慌てる
		{
			get
			{
				return tex.Repa3[13];
			}
		}

		public static string 弁解は続く
		{
			get
			{
				return tex.Repa3[14];
			}
		}

		public static string 物語は終わった
		{
			get
			{
				return tex.Repa3[15];
			}
		}

		public static string 余韻が響く
		{
			get
			{
				return tex.Repa3[16];
			}
		}

		public static string 余韻に浸る
		{
			get
			{
				return tex.Repa3[17];
			}
		}

		public static string え
		{
			get
			{
				return tex.Repa3[18];
			}
		}

		public static string 家に帰る
		{
			get
			{
				return tex.Repa3[19];
			}
		}

		public static string 祝福してほしいの
		{
			get
			{
				return tex.VBle[0];
			}
		}

		public static string そうよね
		{
			get
			{
				return tex.VBle[1];
			}
		}

		public static string あなたはがんばったもの
		{
			get
			{
				return tex.VBle[2];
			}
		}

		public static string ちゅっ
		{
			get
			{
				return tex.VBle[3];
			}
		}

		public static string ヴィオラに頼む
		{
			get
			{
				return tex.VBle[4];
			}
		}

		public static string キスされる
		{
			get
			{
				return tex.VBle[5];
			}
		}

		public static string ヴィオラに祝福された
		{
			get
			{
				return tex.VBle[6];
			}
		}

		private static string[] Race = (from e in (Sta.CurrentDirectory + "\\text\\System\\Race.txt").ReadLines()
		where !string.IsNullOrWhiteSpace(e) && !e.StartsWith("//")
		select e).ToArray<string>();

		private static string[] Attr = (from e in (Sta.CurrentDirectory + "\\text\\System\\Attribute.txt").ReadLines()
		where !string.IsNullOrWhiteSpace(e) && !e.StartsWith("//")
		select e).ToArray<string>();

		private static string[] Comm = (from e in (Sta.CurrentDirectory + "\\text\\System\\Common.txt").ReadLines()
		where !string.IsNullOrWhiteSpace(e) && !e.StartsWith("//")
		select e).ToArray<string>();

		private static string[] Base = (from e in (Sta.CurrentDirectory + "\\text\\Basement\\Basement.txt").ReadLines()
		where !string.IsNullOrWhiteSpace(e) && !e.StartsWith("//")
		select e).ToArray<string>();

		private static string[] Trai = (from e in (Sta.CurrentDirectory + "\\text\\Basement\\Training\\Training.txt").ReadLines()
		where !string.IsNullOrWhiteSpace(e) && !e.StartsWith("//")
		select e).ToArray<string>();

		private static string[] Targ = (from e in (Sta.CurrentDirectory + "\\text\\Basement\\Target.txt").ReadLines()
		where !string.IsNullOrWhiteSpace(e) && !e.StartsWith("//")
		select e).ToArray<string>();

		private static string[] Bles = (from e in (Sta.CurrentDirectory + "\\text\\Basement\\Blessing.txt").ReadLines()
		where !string.IsNullOrWhiteSpace(e) && !e.StartsWith("//")
		select e).ToArray<string>();

		private static string[] Offi = (from e in (Sta.CurrentDirectory + "\\text\\Office\\Office.txt").ReadLines()
		where !string.IsNullOrWhiteSpace(e) && !e.StartsWith("//")
		select e).ToArray<string>();

		private static string[] Bebt = (from e in (Sta.CurrentDirectory + "\\text\\Office\\Bebt.txt").ReadLines()
		where !string.IsNullOrWhiteSpace(e) && !e.StartsWith("//")
		select e).ToArray<string>();

		private static string[] Slav = (from e in (Sta.CurrentDirectory + "\\text\\Office\\Slave.txt").ReadLines()
		where !string.IsNullOrWhiteSpace(e) && !e.StartsWith("//")
		select e).ToArray<string>();

		private static string[] Tool = (from e in (Sta.CurrentDirectory + "\\text\\Office\\Tool.txt").ReadLines()
		where !string.IsNullOrWhiteSpace(e) && !e.StartsWith("//")
		select e).ToArray<string>();

		private static string[] OP0 = (from e in (Sta.CurrentDirectory + "\\text\\Event\\OP0.txt").ReadLines()
		where !string.IsNullOrWhiteSpace(e) && !e.StartsWith("//")
		select e).ToArray<string>();

		private static string[] OP1 = (from e in (Sta.CurrentDirectory + "\\text\\Event\\OP1.txt").ReadLines()
		where !string.IsNullOrWhiteSpace(e) && !e.StartsWith("//")
		select e).ToArray<string>();

		private static string[] Desc = (from e in (Sta.CurrentDirectory + "\\text\\Event\\Description.txt").ReadLines()
		where !string.IsNullOrWhiteSpace(e) && !e.StartsWith("//")
		select e).ToArray<string>();

		private static string[] Firs = (from e in (Sta.CurrentDirectory + "\\text\\Event\\First office.txt").ReadLines()
		where !string.IsNullOrWhiteSpace(e) && !e.StartsWith("//")
		select e).ToArray<string>();

		private static string[] Repa1 = (from e in (Sta.CurrentDirectory + "\\text\\Event\\Repayment1.txt").ReadLines()
		where !string.IsNullOrWhiteSpace(e) && !e.StartsWith("//")
		select e).ToArray<string>();

		private static string[] Repa2 = (from e in (Sta.CurrentDirectory + "\\text\\Event\\Repayment2.txt").ReadLines()
		where !string.IsNullOrWhiteSpace(e) && !e.StartsWith("//")
		select e).ToArray<string>();

		private static string[] Repa3 = (from e in (Sta.CurrentDirectory + "\\text\\Event\\Repayment3.txt").ReadLines()
		where !string.IsNullOrWhiteSpace(e) && !e.StartsWith("//")
		select e).ToArray<string>();

		private static string[] VBle = (from e in (Sta.CurrentDirectory + "\\text\\Event\\Blessing.txt").ReadLines()
		where !string.IsNullOrWhiteSpace(e) && !e.StartsWith("//")
		select e).ToArray<string>();
	}
}

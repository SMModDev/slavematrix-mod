﻿using System;
using System.Collections.Generic;
using _2DGAMELIB;

namespace SlaveMatrix
{
	public struct sep
	{
		public Ele Sta;

		public Ele Ele;

		public Par Par;

		public List<int> Path;

		public Vector2D Pos;
	}
}

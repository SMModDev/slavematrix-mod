﻿using System;

namespace SlaveMatrix
{
	[Serializable]
	public struct ブ\u30FCツ_足情報
	{
		public void SetDefault()
		{
			this.ブ\u30FCツ0_靴底_表示 = true;
			this.ブ\u30FCツ0_ヒ\u30FCル_表示 = true;
			this.ブ\u30FCツ1_タン_表示 = true;
			this.ブ\u30FCツ1_バンプ_バンプ_表示 = true;
			this.ブ\u30FCツ1_バンプ_縁_縁1_表示 = true;
			this.ブ\u30FCツ1_バンプ_縁_縁2_表示 = true;
			this.ブ\u30FCツ1_バンプ_縁_縁3_表示 = true;
			this.ブ\u30FCツ1_バンプ_縁_縁4_表示 = true;
			this.ブ\u30FCツ1_ハイライト_表示 = true;
			this.ブ\u30FCツ1_柄_表示 = true;
			this.ブ\u30FCツ1_紐_紐1_紐下_金具上_金具_表示 = true;
			this.ブ\u30FCツ1_紐_紐1_紐下_金具上_穴_表示 = true;
			this.ブ\u30FCツ1_紐_紐1_紐下_紐_表示 = true;
			this.ブ\u30FCツ1_紐_紐1_紐上_金具上_金具_表示 = true;
			this.ブ\u30FCツ1_紐_紐1_紐上_金具上_穴_表示 = true;
			this.ブ\u30FCツ1_紐_紐1_紐上_紐_表示 = true;
			this.ブ\u30FCツ1_紐_紐2_紐下_金具上_金具_表示 = true;
			this.ブ\u30FCツ1_紐_紐2_紐下_金具上_穴_表示 = true;
			this.ブ\u30FCツ1_紐_紐2_紐下_紐_表示 = true;
			this.ブ\u30FCツ1_紐_紐2_紐上_金具上_金具_表示 = true;
			this.ブ\u30FCツ1_紐_紐2_紐上_金具上_穴_表示 = true;
			this.ブ\u30FCツ1_紐_紐2_紐上_紐_表示 = true;
			this.ブ\u30FCツ1_紐_紐3_紐下_金具上_金具_表示 = true;
			this.ブ\u30FCツ1_紐_紐3_紐下_金具上_穴_表示 = true;
			this.ブ\u30FCツ1_紐_紐3_紐下_紐_表示 = true;
			this.ブ\u30FCツ1_紐_紐3_紐上_金具上_金具_表示 = true;
			this.ブ\u30FCツ1_紐_紐3_紐上_金具上_穴_表示 = true;
			this.ブ\u30FCツ1_紐_紐3_紐上_紐_表示 = true;
			this.ブ\u30FCツ1_紐_紐4_紐下_金具上_金具_表示 = true;
			this.ブ\u30FCツ1_紐_紐4_紐下_金具上_穴_表示 = true;
			this.ブ\u30FCツ1_紐_紐4_紐下_紐_表示 = true;
			this.ブ\u30FCツ1_紐_紐4_紐上_金具上_金具_表示 = true;
			this.ブ\u30FCツ1_紐_紐4_紐上_金具上_穴_表示 = true;
			this.ブ\u30FCツ1_紐_紐4_紐上_紐_表示 = true;
			this.ブ\u30FCツ1_紐_紐5_金具1_金具_表示 = true;
			this.ブ\u30FCツ1_紐_紐5_金具1_穴_表示 = true;
			this.ブ\u30FCツ1_紐_紐5_金具2_金具_表示 = true;
			this.ブ\u30FCツ1_紐_紐5_金具2_穴_表示 = true;
			this.ブ\u30FCツ1_紐_紐5_紐_表示 = true;
		}

		public bool ブ\u30FCツ0_靴底_表示;

		public bool ブ\u30FCツ0_ヒ\u30FCル_表示;

		public bool ブ\u30FCツ1_タン_表示;

		public bool ブ\u30FCツ1_バンプ_バンプ_表示;

		public bool ブ\u30FCツ1_バンプ_縁_縁1_表示;

		public bool ブ\u30FCツ1_バンプ_縁_縁2_表示;

		public bool ブ\u30FCツ1_バンプ_縁_縁3_表示;

		public bool ブ\u30FCツ1_バンプ_縁_縁4_表示;

		public bool ブ\u30FCツ1_ハイライト_表示;

		public bool ブ\u30FCツ1_柄_表示;

		public bool ブ\u30FCツ1_紐_紐1_紐下_金具上_金具_表示;

		public bool ブ\u30FCツ1_紐_紐1_紐下_金具上_穴_表示;

		public bool ブ\u30FCツ1_紐_紐1_紐下_紐_表示;

		public bool ブ\u30FCツ1_紐_紐1_紐上_金具上_金具_表示;

		public bool ブ\u30FCツ1_紐_紐1_紐上_金具上_穴_表示;

		public bool ブ\u30FCツ1_紐_紐1_紐上_紐_表示;

		public bool ブ\u30FCツ1_紐_紐2_紐下_金具上_金具_表示;

		public bool ブ\u30FCツ1_紐_紐2_紐下_金具上_穴_表示;

		public bool ブ\u30FCツ1_紐_紐2_紐下_紐_表示;

		public bool ブ\u30FCツ1_紐_紐2_紐上_金具上_金具_表示;

		public bool ブ\u30FCツ1_紐_紐2_紐上_金具上_穴_表示;

		public bool ブ\u30FCツ1_紐_紐2_紐上_紐_表示;

		public bool ブ\u30FCツ1_紐_紐3_紐下_金具上_金具_表示;

		public bool ブ\u30FCツ1_紐_紐3_紐下_金具上_穴_表示;

		public bool ブ\u30FCツ1_紐_紐3_紐下_紐_表示;

		public bool ブ\u30FCツ1_紐_紐3_紐上_金具上_金具_表示;

		public bool ブ\u30FCツ1_紐_紐3_紐上_金具上_穴_表示;

		public bool ブ\u30FCツ1_紐_紐3_紐上_紐_表示;

		public bool ブ\u30FCツ1_紐_紐4_紐下_金具上_金具_表示;

		public bool ブ\u30FCツ1_紐_紐4_紐下_金具上_穴_表示;

		public bool ブ\u30FCツ1_紐_紐4_紐下_紐_表示;

		public bool ブ\u30FCツ1_紐_紐4_紐上_金具上_金具_表示;

		public bool ブ\u30FCツ1_紐_紐4_紐上_金具上_穴_表示;

		public bool ブ\u30FCツ1_紐_紐4_紐上_紐_表示;

		public bool ブ\u30FCツ1_紐_紐5_金具1_金具_表示;

		public bool ブ\u30FCツ1_紐_紐5_金具1_穴_表示;

		public bool ブ\u30FCツ1_紐_紐5_金具2_金具_表示;

		public bool ブ\u30FCツ1_紐_紐5_金具2_穴_表示;

		public bool ブ\u30FCツ1_紐_紐5_紐_表示;
	}
}

﻿using System;

namespace _2DGAMELIB
{
	public enum AudioPlayMode
	{
		WaitToComplete,
		Background,
		BackgroundLoop
	}
}

﻿using System;

namespace _2DGAMELIB
{
	public enum EncClass
	{
		DESCSP,
		RC2CSP,
		TDESCSP,
		RM,
		ACSP,
		AM
	}
}
